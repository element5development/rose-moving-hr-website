<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<section class="page-title title-section" style="background-image: url('<?php the_field('header_image'); ?>');">
	<div class="block">

		<div class="title-container" style="background-image: url('<?php echo get_template_directory_uri(); ?>/dist/images/title-image.png');">
			<?php if ( is_home() || is_archive() ) { ?>
				<h1>Blog</h1>
			<?php } else if ( is_search() ) { ?>
				<h1>Search: <?php echo get_search_query(); ?></h1>
			<?php } else { ?>
				<h1><?php the_title(); ?></h1>
			<?php } ?>
		</div>

	</div>
</section>