<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<article class="image-card card">

	<?php if ( get_field('header_image') ) { ?>
		<div class="card-image" style="background-image: url('<?php the_field('header_image'); ?>');"></div>
	<?php } else { ?>
		<div class="card-image" style="background-image: url('<?php echo get_template_directory_uri(); ?>/dist/images/default-image.jpg');"></div>
	<?php } ?>
	<h3><?php the_title(); ?></h3>
	<?php the_excerpt(); ?>
	<a href="<?php the_permalink(); ?>" class="button">Learn More</a>

</article>