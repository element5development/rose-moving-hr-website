<section class="grid-items card-container">

	<?php if( have_rows('full_cards') ):
		while ( have_rows('full_cards') ) : the_row(); ?>

			<?php if ( get_sub_field('image') ) { ?>
				<div class="full-card card">
					<?php $image = get_sub_field('image'); ?>
					<div class="card-image">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
					<div class="block">
						<?php the_sub_field('contents'); ?>
						<?php if ( get_sub_field('link') ) { $link = get_sub_field('link'); ?>
							<a target="<?php echo $link['target']; ?>" href="<?php echo $link['url']; ?>" class="button is-ghost"><?php echo $link['title']; ?></a>
						<?php } ?>
					</div>
				</div>
			<?php } else { ?>
				<div class="half-card underline-card card">
					<?php the_sub_field('contents'); ?>
					<?php if ( get_sub_field('link') ) { $link = get_sub_field('link'); ?>
						<a target="<?php echo $link['target']; ?>" href="<?php echo $link['url']; ?>" class="button is-ghost"><?php echo $link['title']; ?></a>
					<?php } ?>
				</div>
			<?php } ?>


		<?php endwhile;
	endif; ?>

</section>