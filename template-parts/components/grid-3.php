<section class="grid-items card-container">

<h2 class="news-header">Rose Employee News</h2>

<?php 
$wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish')); ?>
 
<?php if ( $wpb_all_query->have_posts() ) : ?>

	<?php echo do_shortcode('[ajax_load_more container_type="div" post_type="post" posts_per_page="3" scroll="false" button_label="Load More"]'); ?>

	<?php wp_reset_postdata(); ?>

<?php endif; ?>

</section>