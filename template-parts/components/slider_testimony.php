<?php /*

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel vestibulum erat. Aliquam iaculis lectus
sit amet lorem posuere, at feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus, purus nulla 
lobortis diam, eget posuere massa quam a diam. Duis dignissim velit neque, sed faucibus nulla luctus
vitae.  

*/ ?>

<section class="slider-testimony">
	<?php
		$args = array(
			'post_type' => array('testimony'),
			'posts_per_page' => -1,
			'nopaging' => true,
			'ignore_sticky_posts' => true,
		);
		$partners = new WP_Query( $args );
	?>
	<?php if ( $partners->have_posts() ) : ?>
		<div class="slider-quotes">
			<?php while ( $partners->have_posts() ) : $partners->the_post(); ?>
				<div class="slide-quote">
					<div class="block">
						<p><?php the_field('quote'); ?></p>
						<h3><?php the_field('quotee'); ?></h3>
					</div>
				</div>
			<?php endwhile; ?>
		</div>
	<?php endif; ?>
	<?php wp_reset_query(); ?>
</section>