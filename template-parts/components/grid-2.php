<section class="grid-items card-container-2">

<h2>Rose Employee News</h2>

<?php 
$query = new WP_Query(
	array(
		'post_type'=>'post', 
		'post_status'=>'publish',
		'posts_per_page' => '4'
	)
); ?>
 
<?php if ( $query->have_posts() ) : ?>

<?php while ( $query->have_posts() ) : $query->the_post(); ?>
	<a href="<?php the_permalink(); ?>" class="link-card image-card card-2">
		<?php $header_image = get_field('header_image'); ?>
		<?php if ( get_field('header_image') ) : ?>
			<div class="card-image-2" style="background-image: url('<?php echo $header_image; ?>');"></div>
		<?php else: ?>
			<div class="card-image-2" style="background-image: url('<?php echo get_home_url(); ?>/wp-content/themes/starting-point/assets/images/default-blog-header.jpg');"></div>
		<?php endif; ?>
		<div class="card-info-2">
			<h3><?php the_title(); ?></h3>
			<?php the_excerpt(); ?>
			<div class="button small">Read This</div>
		</div>
	</a>
<?php endwhile; ?>

<?php wp_reset_postdata(); ?>

<?php endif; ?>

<a href="<?php echo get_home_url(); ?>/news/" class="button is-ghost">View All</a>

</section>