<a href="<?php the_permalink(); ?>" class="link-card image-card card-3">

<?php $header_image = get_field('header_image'); ?>
<?php if ( get_field('header_image') ) : ?>
	<div class="card-image-3" style="background-image: url('<?php echo $header_image; ?>');"></div>
<?php else: ?>
	<div class="card-image-3" style="background-image: url('<?php echo get_home_url(); ?>/wp-content/themes/starting-point/assets/images/default-blog-header.jpg');"></div>
<?php endif; ?>

	<h3><?php the_title(); ?></h3>
	<p><?php the_field('excerpt'); ?></p>
	<div class="button">Read This</div>
</a>