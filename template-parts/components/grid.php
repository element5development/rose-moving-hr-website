<div class="sticky">
	<a href="#general" class="button small"><?php the_field('general_header'); ?></a>
	<a href="#training" class="button small"><?php the_field('training_header'); ?></a>
	<a href="#pay" class="button small"><?php the_field('pay_header'); ?></a>
	<a href="#time-off" class="button small"><?php the_field('time_off_header'); ?></a>
	<a href="#benefits" class="button small"><?php the_field('benefits_header'); ?></a>
	<a href="#policies" class="button small"><?php the_field('policies_header'); ?></a>
</div>

<section class="grid-items card-container">

<?php $resources = get_field('grid_items'); 
	$args = array(
		'post_type' => array('generalresource'),
		'posts_per_page' => -1,
		'nopaging' => true,
		'ignore_sticky_posts' => true,
	);
	$query = new WP_Query( $args ); ?>
	<h2 id="general"><?php the_field('general_header'); ?></h2>
	<?php if ( $query->have_posts() ) : ?>
		<?php while ( $query->have_posts() ) : $query->the_post(); ?>
			<?php $attachment_id = get_field('resource_file'); ?>
			<?php if ( get_field('resource_file') ) : ?>
			<?php
				$file_url = wp_get_attachment_url( $attachment_id );
				$filesize = filesize( get_attached_file( $attachment_id ) );
				$filesize = size_format($filesize, 2);		
				$path_info = pathinfo( get_attached_file( $attachment_id ) );
			?>
			<a href="<?php echo $file_url; ?>" class="link-card image-card card" target="_blank">
				<div class="card-image" style="background-image: url('<?php the_field('icon'); ?>');"></div>
				<div class="card-info">
					<h3><?php the_title(); ?></h3>
					<p><strong>File Size: </strong><?php echo $filesize; ?></p>
					<p><strong>Format: </strong><span><?php echo $path_info['extension']; ?></span></p>
					<div class="button is-ghost">Download</div>
				</div>
			</a>
			<?php else: ?>
			<?php $resource_link = get_field('resource_link'); ?>
			<a href="<?php echo $resource_link['url']; ?>" class="link-card image-card card" target="<?php echo $resource_link['target']; ?>">
				<div class="card-image" style="background-image: url('<?php the_field('icon'); ?>');"></div>
				<div class="card-info">
					<h3><?php the_title(); ?></h3>
					<p><?php the_field('resource_link_description'); ?></p>
					<div class="button is-ghost">Open</div>
				</div>
			</a>
			<?php endif; ?>
		<?php endwhile; ?>
	<?php endif; ?>
	<?php wp_reset_query(); ?>

	<?php $resources = get_field('grid_items'); 
	$args = array(
		'post_type' => array('training'),
		'posts_per_page' => -1,
		'nopaging' => true,
		'ignore_sticky_posts' => true,
	);
	$query = new WP_Query( $args ); ?>
	<h2 id="training"><?php the_field('training_header'); ?></h2>
	<?php if ( $query->have_posts() ) : ?>
		<?php while ( $query->have_posts() ) : $query->the_post(); ?>
			<?php $attachment_id = get_field('resource_file'); ?>
			<?php if ( get_field('resource_file') ) : ?>
			<?php
				$file_url = wp_get_attachment_url( $attachment_id );
				$filesize = filesize( get_attached_file( $attachment_id ) );
				$filesize = size_format($filesize, 2);		
				$path_info = pathinfo( get_attached_file( $attachment_id ) );
			?>
			<a href="<?php echo $file_url; ?>" class="link-card image-card card" target="_blank">
				<div class="card-image" style="background-image: url('<?php the_field('icon'); ?>');"></div>
				<div class="card-info">
					<h3><?php the_title(); ?></h3>
					<p><strong>File Size: </strong><?php echo $filesize; ?></p>
					<p><strong>Format: </strong><span><?php echo $path_info['extension']; ?></span></p>
					<div class="button is-ghost">Download</div>
				</div>
			</a>
			<?php else: ?>
			<?php $resource_link = get_field('resource_link'); ?>
			<a href="<?php echo $resource_link['url']; ?>" class="link-card image-card card" target="<?php echo $resource_link['target']; ?>">
				<div class="card-image" style="background-image: url('<?php the_field('icon'); ?>');"></div>
				<div class="card-info">
					<h3><?php the_title(); ?></h3>
					<p><?php the_field('resource_link_description'); ?></p>
					<div class="button is-ghost">Open</div>
				</div>
			</a>
			<?php endif; ?>
		<?php endwhile; ?>
	<?php endif; ?>
	<?php wp_reset_query(); ?>

	<?php $resources = get_field('grid_items'); 
	$args = array(
		'post_type' => array('pay'),
		'posts_per_page' => -1,
		'nopaging' => true,
		'ignore_sticky_posts' => true,
	);
	$query = new WP_Query( $args ); ?>
	<h2 id="pay"><?php the_field('pay_header'); ?></h2>
	<?php if ( $query->have_posts() ) : ?>
		<?php while ( $query->have_posts() ) : $query->the_post(); ?>
			<?php $attachment_id = get_field('resource_file'); ?>
			<?php if ( get_field('resource_file') ) : ?>
			<?php
				$file_url = wp_get_attachment_url( $attachment_id );
				$filesize = filesize( get_attached_file( $attachment_id ) );
				$filesize = size_format($filesize, 2);		
				$path_info = pathinfo( get_attached_file( $attachment_id ) );
			?>
			<a href="<?php echo $file_url; ?>" class="link-card image-card card" target="_blank">
				<div class="card-image" style="background-image: url('<?php the_field('icon'); ?>');"></div>
				<div class="card-info">
					<h3><?php the_title(); ?></h3>
					<p><strong>File Size: </strong><?php echo $filesize; ?></p>
					<p><strong>Format: </strong><span><?php echo $path_info['extension']; ?></span></p>
					<div class="button is-ghost">Download</div>
				</div>
			</a>
			<?php else: ?>
			<?php $resource_link = get_field('resource_link'); ?>
			<a href="<?php echo $resource_link['url']; ?>" class="link-card image-card card" target="<?php echo $resource_link['target']; ?>">
				<div class="card-image" style="background-image: url('<?php the_field('icon'); ?>');"></div>
				<div class="card-info">
					<h3><?php the_title(); ?></h3>
					<p><?php the_field('resource_link_description'); ?></p>
					<div class="button is-ghost">Open</div>
				</div>
			</a>
			<?php endif; ?>
		<?php endwhile; ?>
	<?php endif; ?>
	<?php wp_reset_query(); ?>

	<?php $resources = get_field('grid_items'); 
	$args = array(
		'post_type' => array('timeoff'),
		'posts_per_page' => -1,
		'nopaging' => true,
		'ignore_sticky_posts' => true,
	);
	$query = new WP_Query( $args ); ?>
	<h2 id="time-off"><?php the_field('time_off_header'); ?></h2>
	<?php if ( $query->have_posts() ) : ?>
		<?php while ( $query->have_posts() ) : $query->the_post(); ?>
			<?php $attachment_id = get_field('resource_file'); ?>
			<?php if ( get_field('resource_file') ) : ?>
			<?php
				$file_url = wp_get_attachment_url( $attachment_id );
				$filesize = filesize( get_attached_file( $attachment_id ) );
				$filesize = size_format($filesize, 2);		
				$path_info = pathinfo( get_attached_file( $attachment_id ) );
			?>
			<a href="<?php echo $file_url; ?>" class="link-card image-card card" target="_blank">
				<div class="card-image" style="background-image: url('<?php the_field('icon'); ?>');"></div>
				<div class="card-info">
					<h3><?php the_title(); ?></h3>
					<p><strong>File Size: </strong><?php echo $filesize; ?></p>
					<p><strong>Format: </strong><span><?php echo $path_info['extension']; ?></span></p>
					<div class="button is-ghost">Download</div>
				</div>
			</a>
			<?php else: ?>
			<?php $resource_link = get_field('resource_link'); ?>
			<a href="<?php echo $resource_link['url']; ?>" class="link-card image-card card" target="<?php echo $resource_link['target']; ?>">
				<div class="card-image" style="background-image: url('<?php the_field('icon'); ?>');"></div>
				<div class="card-info">
					<h3><?php the_title(); ?></h3>
					<p><?php the_field('resource_link_description'); ?></p>
					<div class="button is-ghost">Open</div>
				</div>
			</a>
			<?php endif; ?>
		<?php endwhile; ?>
	<?php endif; ?>
	<?php wp_reset_query(); ?>

	<h2 id="benefits"><?php the_field('benefits_header'); ?></h2>

	<?php $resources = get_field('grid_items'); 
	$args = array(
		'post_type' => array('benefit'),
		'posts_per_page' => -1,
		'nopaging' => true,
		'ignore_sticky_posts' => true,
		'meta_value'	=> 'retire'
	);
	$query = new WP_Query( $args ); ?>
		<h4><?php the_field('benefits_subheader_1'); ?></h4>
		<?php if ( $query->have_posts() ) : ?>
		<?php while ( $query->have_posts() ) : $query->the_post(); ?>
			<?php $attachment_id = get_field('resource_file'); ?>
			<?php if ( get_field('resource_file') ) : ?>
			<?php
				$file_url = wp_get_attachment_url( $attachment_id );
				$filesize = filesize( get_attached_file( $attachment_id ) );
				$filesize = size_format($filesize, 2);		
				$path_info = pathinfo( get_attached_file( $attachment_id ) );
			?>
			<a href="<?php echo $file_url; ?>" class="link-card image-card card" target="_blank">
				<div class="card-image" style="background-image: url('<?php the_field('icon'); ?>');"></div>
				<div class="card-info">
					<h3><?php the_title(); ?></h3>
					<p><strong>File Size: </strong><?php echo $filesize; ?></p>
					<p><strong>Format: </strong><span><?php echo $path_info['extension']; ?></span></p>
					<div class="button is-ghost">Download</div>
				</div>
			</a>
			<?php else: ?>
			<?php $resource_link = get_field('resource_link'); ?>
			<a href="<?php echo $resource_link['url']; ?>" class="link-card image-card card" target="<?php echo $resource_link['target']; ?>">
				<div class="card-image" style="background-image: url('<?php the_field('icon'); ?>');"></div>
				<div class="card-info">
					<h3><?php the_title(); ?></h3>
					<p><?php the_field('resource_link_description'); ?></p>
					<div class="button is-ghost">Open</div>
				</div>
			</a>
			<?php endif; ?>
		<?php endwhile; ?>
	<?php endif; ?>
	<?php wp_reset_query(); ?>

	<?php $resources = get_field('grid_items'); 
	$args = array(
		'post_type' => array('benefit'),
		'posts_per_page' => -1,
		'nopaging' => true,
		'ignore_sticky_posts' => true,
		'meta_value'	=> 'medical'
	);
	$query = new WP_Query( $args ); ?>
		<h4><?php the_field('benefits_subheader_2'); ?></h4>
		<?php if ( $query->have_posts() ) : ?>
		<?php while ( $query->have_posts() ) : $query->the_post(); ?>
			<?php $attachment_id = get_field('resource_file'); ?>
			<?php if ( get_field('resource_file') ) : ?>
			<?php
				$file_url = wp_get_attachment_url( $attachment_id );
				$filesize = filesize( get_attached_file( $attachment_id ) );
				$filesize = size_format($filesize, 2);		
				$path_info = pathinfo( get_attached_file( $attachment_id ) );
			?>
			<a href="<?php echo $file_url; ?>" class="link-card image-card card" target="_blank">
				<div class="card-image" style="background-image: url('<?php the_field('icon'); ?>');"></div>
				<div class="card-info">
					<h3><?php the_title(); ?></h3>
					<p><strong>File Size: </strong><?php echo $filesize; ?></p>
					<p><strong>Format: </strong><span><?php echo $path_info['extension']; ?></span></p>
					<div class="button is-ghost">Download</div>
				</div>
			</a>
			<?php else: ?>
			<?php $resource_link = get_field('resource_link'); ?>
			<a href="<?php echo $resource_link['url']; ?>" class="link-card image-card card" target="<?php echo $resource_link['target']; ?>">
				<div class="card-image" style="background-image: url('<?php the_field('icon'); ?>');"></div>
				<div class="card-info">
					<h3><?php the_title(); ?></h3>
					<p><?php the_field('resource_link_description'); ?></p>
					<div class="button is-ghost">Open</div>
				</div>
			</a>
			<?php endif; ?>
		<?php endwhile; ?>
	<?php endif; ?>
	<?php wp_reset_query(); ?>

	<?php $resources = get_field('grid_items'); 
	$args = array(
		'post_type' => array('policy'),
		'posts_per_page' => -1,
		'nopaging' => true,
		'ignore_sticky_posts' => true,
	);
	$query = new WP_Query( $args ); ?>
	<h2 id="policies"><?php the_field('policies_header'); ?></h2>
	<?php if ( $query->have_posts() ) : ?>
		<?php while ( $query->have_posts() ) : $query->the_post(); ?>
			<?php $attachment_id = get_field('resource_file'); ?>
			<?php if ( get_field('resource_file') ) : ?>
			<?php
				$file_url = wp_get_attachment_url( $attachment_id );
				$filesize = filesize( get_attached_file( $attachment_id ) );
				$filesize = size_format($filesize, 2);		
				$path_info = pathinfo( get_attached_file( $attachment_id ) );
			?>
			<a href="<?php echo $file_url; ?>" class="link-card image-card card" target="_blank">
				<div class="card-image" style="background-image: url('<?php the_field('icon'); ?>');"></div>
				<div class="card-info">
					<h3><?php the_title(); ?></h3>
					<p><strong>File Size: </strong><?php echo $filesize; ?></p>
					<p><strong>Format: </strong><span><?php echo $path_info['extension']; ?></span></p>
					<div class="button is-ghost">Download</div>
				</div>
			</a>
			<?php else: ?>
			<?php $resource_link = get_field('resource_link'); ?>
			<a href="<?php echo $resource_link['url']; ?>" class="link-card image-card card" target="<?php echo $resource_link['target']; ?>">
				<div class="card-image" style="background-image: url('<?php the_field('icon'); ?>');"></div>
				<div class="card-info">
					<h3><?php the_title(); ?></h3>
					<p><?php the_field('resource_link_description'); ?></p>
					<div class="button is-ghost">Open</div>
				</div>
			</a>
			<?php endif; ?>
		<?php endwhile; ?>
	<?php endif; ?>
	<?php wp_reset_query(); ?>

</section>