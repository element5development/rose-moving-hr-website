<?php /*

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel vestibulum erat. Aliquam iaculis lectus
sit amet lorem posuere, at feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus, purus nulla 
lobortis diam, eget posuere massa quam a diam. Duis dignissim velit neque, sed faucibus nulla luctus
vitae.  

*/ ?>

<section class="slider-partner">
	<?php
		$args = array(
			'post_type' => array('partners'),
			'posts_per_page' => -1,
			'nopaging' => true,
			'ignore_sticky_posts' => true,
		);
		$partners = new WP_Query( $args );
	?>
	<?php if ( $partners->have_posts() ) : ?>
		<div class="slider-logos">
			<?php while ( $partners->have_posts() ) : $partners->the_post(); ?>
				<?php $image = get_field('logo'); ?>
				<div class="slide-partner">
					<div class="block">
						<a target="_blank" href="<?php the_field('website_url'); ?>">
							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</a>
					</div>
				</div>
			<?php endwhile; ?>
		</div>
	<?php endif; ?>
	<?php wp_reset_query(); ?>
</section>