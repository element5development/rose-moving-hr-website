<?php /*

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel vestibulum erat. Aliquam iaculis lectus
sit amet lorem posuere, at feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus, purus nulla 
lobortis diam, eget posuere massa quam a diam. Duis dignissim velit neque, sed faucibus nulla luctus
vitae.  

*/ ?>

<footer class="page-footer">
	<div class="block">
		<div class="block">
			<h2>Rose Moving & Storage</h2>
			<div class="half">
				<p><?php the_field('primary_address','options'); ?><br><?php the_field('primary_city','options'); ?>, <?php the_field('primary_state','options'); ?> <?php the_field('primary_zip','options'); ?>
			</div>
			<div class="half">
				<?php 
					$phone = get_field('primary_phone','options');
					$phone = preg_replace('/[^0-9]/', '', $phone);
					$phone = '+1' . $phone;
				?>
				<p>Phone: <a href="tel:<?php echo $phone; ?>"><?php the_field('primary_phone','options'); ?></a><br>
				Fax: <?php the_field('primary_fax','options'); ?>
			</div>
		</div>
		<div class="block">
			<nav class="social-nav">
				<a target="_blank" href="https://www.facebook.com/RoseMoving/" class="social-button facebook">
					<svg viewBox="0 0 50 50" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414">
						<path d="M32.197 48.32V27.047h7.137l1.07-8.293h-8.207V13.46c0-2.4.664-4.036 4.11-4.036h4.387v-7.42c-.76-.097-3.363-.324-6.395-.324-6.33 0-10.665 3.864-10.665 10.96v6.114h-7.16v8.293h7.16V48.32h8.562z" fill-rule="nonzero"></path>
					</svg>
				</a>
				<a target="_blank" href="https://www.linkedin.com/company/rose-moving-and-storage/" class="social-button linkedin">
				<svg viewBox="0 0 50 50" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414">
					<path d="M43.017 33.175v13.322h-7.723v-12.43c0-3.122-1.117-5.253-3.913-5.253-2.133 0-3.403 1.436-3.962 2.825-.203.496-.256 1.187-.256 1.882v12.975h-7.726s.104-21.052 0-23.233h7.725v3.293c-.014.025-.035.05-.05.075h.05v-.075c1.028-1.58 2.86-3.84 6.963-3.84 5.083 0 8.894 3.322 8.894 10.458zm-31.662-21.11c-2.643 0-4.372 1.733-4.372 4.013 0 2.23 1.68 4.016 4.27 4.016h.05c2.695 0 4.37-1.786 4.37-4.016-.05-2.28-1.675-4.014-4.318-4.014zM7.442 46.496h7.723V23.264H7.442v23.233z" fill-rule="nonzero"></path>
				</svg>
				</a>
				<a target="_blank" href="mailto:rose@rosemoving.com" class="social-button email">
					<svg viewBox="0 0 50 50" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414">
						<path d="M1.34 7.918v34.164h47.32V7.918H1.34zM25 21.658l-13.687-9.124H38.74L25 21.658zM5.956 37.466V14.55l17.77 11.882 1.274.796 1.273-.796 17.77-11.83V37.52H5.957v-.054z" fill-rule="nonzero"></path>
					</svg>
				</a>
			</nav>
			<img src="<?php echo get_template_directory_uri(); ?>/dist/images/WDLN-logo.png"  alt="Women's Business Enterprise"/>
		</div>
	</div>
</footer>