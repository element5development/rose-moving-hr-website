<?php 
/*-------------------------------------------------------------------
		Template Name: News
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/pages/content', 'title'); ?>

<?php if( !empty(get_the_content()) ) { ?>
	<section class="default-contents">
		<?php get_template_part('template-parts/pages/content', 'default'); ?>
	</section>
<?php } ?>

<section class="default-contents">
	<?php get_template_part('template-parts/components/grid-3'); ?>
</section>

<?php get_footer(); ?>