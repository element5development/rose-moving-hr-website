<?php 
/*-------------------------------------------------------------------

Lorem ipsum dolor sit amet, consectetur adipiscing elit. In vel
vestibulum erat. Aliquam iaculis lectus sit amet lorem posuere, at
feugiat arcu imperdiet. Nullam tempor, purus quis aliquam luctus,
purus nulla lobortis diam, eget posuere massa quam a diam. Duis
dignissim velit neque, sed faucibus nulla luctus vitae.  

------------------------------------------------------------------*/
?>

<?php get_header(); ?>

	<?php get_template_part('template-parts/pages/content', 'title'); ?>

	<section class="default-contents">
		<?php if (!have_posts()) : ?>
			<p>Sorry, no results were found</p>
			<?php get_search_form(); ?>
		<?php endif; ?>
		
		<?php while (have_posts()) : the_post(); ?>
			<?php get_template_part( 'template-parts/posts/previews/preview', get_post_type() ); ?>
		<?php endwhile; ?>
	</section>

<?php get_footer(); ?>
