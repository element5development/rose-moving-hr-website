<?php
/*-----------------------------------------
  BUTTON SHORTCODE
-----------------------------------------*/
function cta_button($atts, $content = null) {
  extract( shortcode_atts( array(
    'target' => '',
    'url' => '#',
		'type' => '',
  ), $atts ) );
  $link = '<a target="'.$target.'" href="'.$url.'" class="button is-'.$type.'">' . do_shortcode($content) . '</a>';
  return $link;
}
add_shortcode('btn', 'cta_button');
?>