<?php 
/*-------------------------------------------------------------------
		Template Name: Grid
-------------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/pages/content', 'title'); ?>

<?php if( !empty(get_the_content()) ) { ?>
	<section class="default-contents">
		<?php get_template_part('template-parts/pages/content', 'default'); ?>
	</section>
<?php } ?>

<div class="wrapper">
  <div class="main">
		<?php get_template_part('template-parts/components/grid'); ?>
  </div>
  <aside class="aside aside-1">
		<?php get_template_part('template-parts/components/grid-2'); ?>
	</aside>
</div>

<?php get_footer(); ?>